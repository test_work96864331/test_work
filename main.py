import os
import pandas as pd

from tinkoff.invest import Client, PositionsResponse,RequestError
from config import reedToken

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

def merge_value(v):
    return v.units + v.nano/1e9

if __name__ == '__main__':

    try:
        with Client(reedToken) as client:
            pf = client.users.get_accounts()
            for account in pf.accounts:
                r: PositionsResponse = client.operations.get_portfolio(account_id=account.id)
                df = pd.DataFrame([{
                    'figi':k.figi,
                    'quantity':merge_value(k.quantity),
                    'current_price':merge_value(k.current_price),
                    'average_position_price_fifo':merge_value(k.average_position_price_fifo),
                    'quantity_lots':merge_value(k.quantity_lots)
                }for k in r.positions])
                print(df.head(100))

    except RequestError as e:
        print(e)




