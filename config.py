import os
from dotenv import load_dotenv

load_dotenv()
reedToken = os.getenv('REED_TOKEN')